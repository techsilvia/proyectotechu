var express = require('express');
var cors = require('cors');
  app = express();
  app.use(cors());
  port = process.env.PORT || 3000;
  console.log(port);

//variable para poder usar el request-json
var requestjson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json

var path = require('path');

// MONGODB
var mongoClient = require('mongodb').MongoClient;
//var url ="mongodb://localhost:27017/local";
//var url ="mongodb://172.18.0.2:27017/local";
//var url mondodb - servermongo docker container
var url ="mongodb://servermongo:27017/local";


// MLAB
var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos";
var apiKey = "apiKey=jEx6OlQOJmUHZeZBARrkqhO-eEeuOlTK";
var clienteMlab = requestjson.createClient(urlmovimientosMlab + apiKey);

// POSTGRE
var pg = require('pg');
//var urlUsusarios = "postgres://docker:docker@localhost:5433/bdseguridad";
var urlUsusarios = "postgres://alumno@postbootcamp:TechU2017TechU2017@postbootcamp.postgres.database.azure.com:5432/bdseguridad";
var clientePostgre = new pg.Client (urlUsusarios);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

//MONGODB (UserId - movements list )

app.get('/user/:userId/movements', function(req, res){
  console.log('get '+ req.params.userId);
  mongoClient.connect(url, function(err, db){
    if (err){
      console.log(err);
      res.send({'err': err});
    }
    else {
      var col = db.collection('movimientos');
      console.log("Connected successfully to server");
      col.find({"iduser":req.params.userId}).toArray(function(err, docs){
        JSON.stringify(docs)
        //en el front al recuperar JSON.parse(xxxxxx)
        //docs.length
        res.send(docs);
      });
      db.close();
    }
  })
});

//MONGODB (insert a movement)

app.post('/movement', function(req, res) {
  console.log('post '+ req.body);
  mongoClient.connect(url,function(err, db){
    if (err){
      console.log(err);
      res.send(err);
    }
    else{
      console.log("traying to insert to server");
      var col = db.collection('movimientos');

      col.insertOne(req.body, function(err, r){
        console.log(r.insertedCount + 'registros insertados');
        res.send(r);
      });
      db.close();
    }
  })
});



//MONGODB (UserId - movements list )

app.get('/user/:userId/accounts', function(req, res){
  console.log('get '+ req.params.userId);
  mongoClient.connect(url, function(err, db){
    if (err){
      console.log(err);
      res.send({'err': err});
    }
    else {
      var col = db.collection('cuentas');
      console.log("Connected successfully to server desde cuentas");
      col.find({"iduser":req.params.userId}).toArray(function(err, docs){
        //en el front al recuperar JSON.parse
        JSON.stringify(docs)
        res.send(docs);
      });
      db.close();
    }
  })
});

/*
db.collection('prueba').insertMany([{a:2,a:3}]), function(err, r){
  console.log(r.insertedCount + 'registros insertados');
});

db.collection('prueba').insertOne(req.body), function(err, r){
  console.log(r.insertedCount + 'registros insertados');
});
*/


// postgres bbdd usuarios

clientePostgre.connect();

//req.body = {usuario:xxxx, password:yyyy}
//Ejecuto la query de consulta

app.post('/login',function(req,res){
  const query = clientePostgre.query('SELECT COUNT(*) FROM usuarios WHERE login=$1 AND password=$2;', [req.body.login, req.body.password],(err,result) => {
    if (err){
      console.log(err);
      res.send(err);
    }
    else {
      if (result.rows[0].count >= 1){
        res.send("true");
      }
      else {
        res.send("false");
      }
    }
  });
});

app.post('/register',function(req,res){
  const query = clientePostgre.query('INSERT INTO usuarios (login,password,nombre,apellidos,email) VALUES ($1,$2,$3,$4,$5);', [req.body.login,req.body.password,req.body.nombre,req.body.apellidos,req.body.email],(err,result) => {
  if (err){
    console.log(err);
    res.send(err);
  }
  else {
    if (result.rowCount == 1){
      res.send("true del backtechuarranz");
    }
    else {
      res.send("false");
    }
  }
});
//devolver el resultados
});



app.get('/movimientos', function(req, res) {
  clienteMlab = requestjson.createClient(urlmovimientosMlab + "?" + apiKey);
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.get('/clientes', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteMlab = requestjson.createClient(urlmovimientosMlab + "?f={'idcliente':1, 'nombre': 1, 'apellidos': 1}" + "&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      console.log(body);
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.post('/movimientos', function(req, res) {
  /* Crear movimiento en MLab */
  clienteMlab = requestjson.createClient(urlmovimientosMlab + "?" + apiKey);
  clienteMlab.post('',req.body,function(err, resM, body){
  if(err)
  {
    console.log(body);
  }
  else
  {
    res.send(body);
  }
});
});

app.get('/movimientos/:idcliente', function (req, res) {
  /* Obtener movimientos del cliente idcliente desde MLab */
})
