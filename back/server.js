var express = require('express');
var cors = require('cors');
app = express();
app.use(cors());
port = process.env.PORT || 3000;
console.log(port);

//for using request-json
var requestjson = require('request-json');
//for parsing req.body in case undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json

var path = require('path');

// MONGODB
var mongoClient = require('mongodb').MongoClient;
var url ="mongodb://servermongo:27017/local";


// MLAB
var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos";
var apiKey = "apiKey=jEx6OlQOJmUHZeZBARrkqhO-eEeuOlTK";
var clienteMlab = requestjson.createClient(urlmovimientosMlab + apiKey);

// POSTGRE
var pg = require('pg');
var urlUsusarios = "postgres://alumno@postbootcamp:TechU2017TechU2017@postbootcamp.postgres.database.azure.com:5432/bdseguridad";
var clientePostgre = new pg.Client (urlUsusarios);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

//MONGODB (/user/:userId/movements)

app.get('/user/:userId/movements', function(req, res){
  mongoClient.connect(url, function(err, db){
    if (err){
      console.log(err);
      res.send({'err': err});
    }
    else {
      var col = db.collection('movimientos');
      col.find({"iduser":req.params.userId}).toArray(function(err, docs){
        JSON.stringify(docs)
        res.send(docs);
      });
      db.close();
    }
  })
});

//MONGODB (/movement)

app.post('/movement', function(req, res) {
  mongoClient.connect(url,function(err, db){
    if (err){
      console.log(err);
      res.send(err);
    }
    else{
      var col = db.collection('movimientos');
      col.insertOne(req.body, function(err, r){
        if (r.insertedCount == 1) {
          res.send("true")
        }
        else {
          res.send("false")

        };
      });
      db.close();
    }
  })
});



//MONGODB (/user/:userId/accounts )

app.get('/user/:userId/accounts', function(req, res){
  console.log('get '+ req.params.userId);
  mongoClient.connect(url, function(err, db){
    if (err){
      console.log(err);
      res.send({'err': err});
    }
    else {
      var col = db.collection('cuentas');
      col.find({"iduser":req.params.userId}).toArray(function(err, docs){
        JSON.stringify(docs)
        res.send(docs);
      });
      db.close();
    }
  })
});


// POSTGRE (/login)

clientePostgre.connect();
console.log("dentro del back y antes del post");

app.post('/login',function(req,res){
  const query = clientePostgre.query('SELECT COUNT(*) FROM usuarios WHERE login=$1 AND password=$2;', [req.body.login, req.body.password],(err,result) => {
    if (err){
      console.log("error" +err);
      res.send(err);
    }
    else {
      if (result.rows[0].count >= 1){
        console.log("result.rows.count mayor que 1");
        res.send("true");
      }
      else {
            console.log("result.rows.count menor que 1");
        res.send("false");
      }
    }
  });
});

app.post('/register',function(req,res){
  const query = clientePostgre.query('INSERT INTO usuarios (login,password,nombre,apellidos,email) VALUES ($1,$2,$3,$4,$5);', [req.body.login,req.body.password,req.body.nombre,req.body.apellidos,req.body.email],(err,result) => {
    if (err){
      console.log(err);
      res.send(err);
    }
    else {
      if (result.rowCount == 1){
        res.send("true");
      }
      else {
        res.send("false");
      }
    }
  });
});


//MLAB (/movimientos)

app.get('/movimientos', function(req, res) {
  clienteMlab = requestjson.createClient(urlmovimientosMlab + "?" + apiKey);
  clienteMlab.get('', function(err, resM, body) {
    if(err)
    {
      console.log(body);
    }
    else
    {
      res.send(body);
    }
  });
});

//MLAB (/clientes)

app.get('/clientes', function(req, res) {

  clienteMlab = requestjson.createClient(urlmovimientosMlab + "?f={'idcliente':1, 'nombre': 1, 'apellidos': 1}" + "&" + apiKey);
  clienteMlab.get('', function(err, resM, body) {
    if(err)
    {
      console.log(body);
    }
    else
    {
      res.send(body);
    }
  });
});

//MLAB (/movimientos)
app.post('/movimientos', function(req, res) {
  clienteMlab = requestjson.createClient(urlmovimientosMlab + "?" + apiKey);
  clienteMlab.post('',req.body,function(err, resM, body){
    if(err)
    {
      console.log(body);
    }
    else
    {
      res.send(body);
    }
  });
});
